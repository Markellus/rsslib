﻿using System;
using System.Xml;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Threading;

namespace Mynix.RSSLib
{
    /// <summary>
    /// Wandelt (XML)-RSS-Feeds in die C#-Klassenstruktur um.
    /// </summary>
    [Serializable]
    public class RssManager : IDisposable
    {
        private bool isDisposed;
        private bool isAsync = false;

        /// <summary>
        /// Die Url, von der der Feed geladen wurde.
        /// </summary>
        public string Url { get; private set; }

        /// <summary>
        /// Enthält alle Einträge des Feeds.
        /// </summary>
        public List<RSSItem> RSSItems { get; private set; }

        /// <summary>
        /// Der Titel des Feeds.
        /// </summary>
        public string FeedTitle { get; private set; }

        /// <summary>
        /// Die Beschreibung des Feeds.
        /// </summary>
        public string FeedDescription { get; private set; }



        /// <summary>
        /// Wird aufgerufen, wenn der Feed gedownloaded und fertig geparst wurde.
        /// </summary>
        public event EventHandler<EventArgs> FeedReady;



        /// <summary>
        /// Erstellt einen neuen RSS-Manager für den Feed unter der angegebenen Url.
        /// </summary>
        /// <param name="feedUrl">Die Url des Feeds</param>
        public RssManager(string feedUrl)
        {
            Url = feedUrl;
            RSSItems = new List<RSSItem>();
        }

        /// <summary>
        /// Läd den Feed herunter.
        /// </summary>
        /// <param name="loadAsync">Wenn true, blockiert das Downloaden des Feeds nicht den derzeitigen Thread.</param>
        public void LoadFeed(bool loadAsync)
        {
            isAsync = loadAsync;

            if (loadAsync)
            {
                Thread t = new Thread(() => RssManagerAsync(Url));
                t.Start();
            }
            else
            {
                GetFeed();
                FireReadyEvent(this);
            }
        }

        /// <summary>
        /// Diese Methode wird in einem separaten Thread ausgeführt.
        /// </summary>
        /// <param name="feedUrl"></param>
        private void RssManagerAsync(string feedUrl)
        {
            RssManager m = new RssManager(feedUrl);
            m.LoadFeed(false);
            FireReadyEvent(m);
        }

        /// <summary>
        /// Läd den Feed und parst ihn.
        /// </summary>
        private void GetFeed()
        {
            if (String.IsNullOrEmpty(Url))
            {
                throw new ArgumentException("You must provide a feed URL");
            }

            using (XmlReader reader = XmlReader.Create(Url))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(reader);
                FeedTitle = ParseDocElements(xmlDoc.SelectSingleNode("//channel"), "title");
                FeedDescription = ParseDocElements(xmlDoc.SelectSingleNode("//channel"), "description");
                ParseRSSItem(xmlDoc);
            }
        }

        /// <summary>
        /// Parses the xml document in order to retrieve the RSS items.
        /// </summary>
        private void ParseRSSItem(XmlDocument xmlDoc)
        {
            RSSItems.Clear();
            XmlNodeList nodes = xmlDoc.SelectNodes("rss/channel/item");

            foreach (XmlNode node in nodes)
            {
                RSSItem item = new RSSItem();
                item.Title = ParseDocElements(node, "title");
                item.Description = ParseDocElements(node, "description");
                item.Link = ParseDocElements(node, "link");

                string date =  ParseDocElements(node, "pubDate");
                DateTime.TryParse(date, out item.Date);

                RSSItems.Add(item);
            }
        }

        /// <summary>
        /// Parses the XmlNode with the specified XPath query 
        /// and assigns the value to the property parameter.
        /// </summary>
        private string ParseDocElements(XmlNode parent, string xPath)
        {
            string property = "";

            XmlNode node = parent.SelectSingleNode(xPath);
            if (node != null)
                property = node.InnerText;
            else
                property = "Unresolvable";

            return property;
        }


        /// <summary>
        /// Löst das FeedReady-Event aus.
        /// </summary>
        private void FireReadyEvent(RssManager manager)
        {
            if (isAsync)
            {
                this.FeedTitle = manager.FeedTitle;
                this.FeedDescription = manager.FeedDescription;
                this.Url = manager.Url;
                this.RSSItems = manager.RSSItems;
            }
            

            EventArgs e = new EventArgs();
            EventHandler<EventArgs> local;

            lock (this)
            {
                local = FeedReady;
            }
            if (local != null)
            {
                local(this, e);
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs the disposal.
        /// </summary>
        private void Dispose(bool disposing)
        {
            if (disposing && !isDisposed)
            {
                RSSItems.Clear();
                Url = null;
                FeedTitle = null;
                FeedDescription = null;
            }

            isDisposed = true;
        }

        /// <summary>
        /// Releases the object to the garbage collector
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}