﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mynix.RSSLib
{

    /// <summary>
    /// A structure to hold the RSS Feed items
    /// </summary>
    [Serializable]
    public struct RSSItem
    {
        /// <summary>
        /// Das Veröffentlichungsdatum
        /// </summary>
        public DateTime Date;

        /// <summary>
        /// Der Titel des Eintrags
        /// </summary>
        public string Title;

        /// <summary>
        /// Die Beschreibung
        /// </summary>
        public string Description;

        /// <summary>
        /// Ein Link zum vollständigen Artikel
        /// </summary>
        public string Link;
    }
}
